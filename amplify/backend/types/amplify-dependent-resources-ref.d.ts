export type AmplifyDependentResourcesAttributes = {
  "api": {
    "amplifygitlabci": {
      "GraphQLAPIEndpointOutput": "string",
      "GraphQLAPIIdOutput": "string",
      "GraphQLAPIKeyOutput": "string"
    }
  }
}